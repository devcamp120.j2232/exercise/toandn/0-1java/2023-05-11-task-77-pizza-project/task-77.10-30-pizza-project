package com.devcamp.pizza365.repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.model.Country;

public interface ICountryRepository extends JpaRepository<Country, Integer> {

	@Query(value = "SELECT * FROM countries WHERE country_name LIKE :countryName%", nativeQuery = true)
	List<Country> findCountryByCountryNameLike(@Param("countryName") String countryName);

}

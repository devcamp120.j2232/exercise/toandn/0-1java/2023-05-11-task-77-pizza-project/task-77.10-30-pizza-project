package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.pizza365.entity.Office;
import com.devcamp.pizza365.repository.IOfficeRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class OfficeService {
  @Autowired
  IOfficeRepository gIOfficeRepository;

  public ArrayList<Office> getAllOffice() {
    ArrayList<Office> listOffice = new ArrayList<>();
    gIOfficeRepository.findAll().forEach(listOffice::add);
    return listOffice;
  }

  public Office createEmployee(Office pOffice) {
    try {
      Office vOfficeSave = gIOfficeRepository.save(pOffice);
      return vOfficeSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Office updateOffice(Office pOffice, Optional<Office> pOfficeData) {
    try {
      Office vOffice = pOfficeData.get();
      vOffice.setCity(pOffice.getCity());
      vOffice.setPhone(pOffice.getPhone());
      vOffice.setAddressLine(pOffice.getAddressLine());
      vOffice.setState(pOffice.getState());
      vOffice.setCountry(pOffice.getCountry());
      vOffice.setTerritory(pOffice.getTerritory());
      Office vOfficeSave = gIOfficeRepository.save(vOffice);
      return vOfficeSave;
    } catch (Exception e) {
      return null;
    }
  }

}

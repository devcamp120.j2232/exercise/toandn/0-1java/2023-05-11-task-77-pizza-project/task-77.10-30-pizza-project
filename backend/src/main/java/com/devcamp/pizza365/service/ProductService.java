package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.IProductRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
  @Autowired
  IProductRepository gIProductRepository;

  public ArrayList<Product> getAllProduct() {
    ArrayList<Product> listProduct = new ArrayList<>();
    gIProductRepository.findAll().forEach(listProduct::add);
    return listProduct;
  }

  public Product createProduct(Product pProduct,
      Optional<ProductLine> pProductLineData) {
    try {
      Product vProduct = new Product();
      vProduct.setProductCode(pProduct.getProductCode());
      vProduct.setProductName(pProduct.getProductName());
      vProduct.setProductDescripttion(pProduct.getProductDescripttion());
      vProduct.setProductLine(pProductLineData.get());
      vProduct.setProductScale(pProduct.getProductScale());
      vProduct.setProductVendor(pProduct.getProductVendor());
      vProduct.setQuantityInStock(pProduct.getQuantityInStock());
      vProduct.setBuyPrice(pProduct.getBuyPrice());
      Product vProductSave = gIProductRepository.save(vProduct);
      return vProductSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Product updateOffice(Product pProduct, Optional<Product> pProductData,
      Optional<ProductLine> pProductLineData) {
    try {
      Product vProduct = pProductData.get();
      vProduct.setProductCode(pProduct.getProductCode());
      vProduct.setProductName(pProduct.getProductName());
      vProduct.setProductDescripttion(pProduct.getProductDescripttion());
      vProduct.setProductLine(pProductLineData.get());
      vProduct.setProductScale(pProduct.getProductScale());
      vProduct.setProductVendor(pProduct.getProductVendor());
      vProduct.setQuantityInStock(pProduct.getQuantityInStock());
      vProduct.setBuyPrice(pProduct.getBuyPrice());
      Product vProductSave = gIProductRepository.save(vProduct);
      return vProductSave;
    } catch (Exception e) {
      return null;
    }
  }

}

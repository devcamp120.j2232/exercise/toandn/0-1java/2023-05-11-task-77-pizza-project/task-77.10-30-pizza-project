package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.pizza365.entity.OrderDetail;
import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.repository.IOrderDetailRepository;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailService {
  @Autowired
  IOrderDetailRepository gIOrderDetailRepository;

  public ArrayList<OrderDetail> getAllOrderDetail() {
    ArrayList<OrderDetail> listOrderDetail = new ArrayList<>();
    gIOrderDetailRepository.findAll().forEach(listOrderDetail::add);
    return listOrderDetail;
  }

  public OrderDetail createOrderDetail(OrderDetail pOrderDetail,
      Optional<Order> pOrderData, Optional<Product> pProductData) {
    try {
      OrderDetail vOrderDetail = new OrderDetail();
      vOrderDetail.setQuantityOrder(pOrderDetail.getQuantityOrder());
      vOrderDetail.setPriceEach(pOrderDetail.getPriceEach());
      vOrderDetail.setProduct(pProductData.get());
      vOrderDetail.setOrder(pOrderData.get());
      OrderDetail vOrderDetailSave = gIOrderDetailRepository.save(vOrderDetail);
      return vOrderDetailSave;
    } catch (Exception e) {
      return null;
    }
  }

  public OrderDetail updateOrderDetail(OrderDetail pOrderDetail, Optional<OrderDetail> pOrderDetailData,
      Optional<Order> pOrderData, Optional<Product> pProductData) {
    try {
      OrderDetail vOrderDetail = pOrderDetailData.get();
      vOrderDetail.setQuantityOrder(pOrderDetail.getQuantityOrder());
      vOrderDetail.setPriceEach(pOrderDetail.getPriceEach());
      vOrderDetail.setProduct(pProductData.get());
      vOrderDetail.setOrder(pOrderData.get());
      OrderDetail vOrderDetailSave = gIOrderDetailRepository.save(vOrderDetail);
      return vOrderDetailSave;
    } catch (Exception e) {
      return null;
    }
  }
}

package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.repository.ICustomerRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
  @Autowired
  ICustomerRepository gICustomerRepository;

  public ArrayList<Customer> getAllCustomer() {
    ArrayList<Customer> listCustomer = new ArrayList<>();
    gICustomerRepository.findAll().forEach(listCustomer::add);
    return listCustomer;
  }

  public Customer createCustomer(Customer pCustomer) {
    try {
      Customer vCustomerSave = gICustomerRepository.save(pCustomer);
      return vCustomerSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Customer updateCustomer(Customer pCustomer, Optional<Customer> pCustomerData) {
    try {
      Customer vCustomer = pCustomerData.get();
      vCustomer.setLastName(pCustomer.getLastName());
      vCustomer.setFirstName(pCustomer.getFirstName());
      vCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
      vCustomer.setAddress(pCustomer.getAddress());
      vCustomer.setCity(pCustomer.getCity());
      vCustomer.setState(pCustomer.getState());
      vCustomer.setPostalCode(pCustomer.getPostalCode());
      vCustomer.setCountry(pCustomer.getCountry());
      vCustomer.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
      vCustomer.setCreditLimit(pCustomer.getCreditLimit());
      Customer vCustomerSave = gICustomerRepository.save(vCustomer);
      return vCustomerSave;
    } catch (Exception e) {
      return null;
    }
  }
}

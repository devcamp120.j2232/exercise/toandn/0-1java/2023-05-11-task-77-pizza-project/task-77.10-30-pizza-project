package com.devcamp.pizza365.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.*;
import com.devcamp.pizza365.model.ExistsData;
import com.devcamp.pizza365.repository.*;
import com.devcamp.pizza365.service.ProductService;

@RestController
@CrossOrigin
@RequestMapping("/product")
public class ProductController {
	@Autowired
	IProductRepository gIProductRepository;
	@Autowired
	IProductLineRepository gProductLineRepository;
	@Autowired
	ProductService gProductService;

	@GetMapping("/all")
	public ResponseEntity<List<Product>> getAllProduct() {
		try {
			return new ResponseEntity<>(gProductService.getAllProduct(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/details/{id}")
	public ResponseEntity<Object> getProductById(@PathVariable Integer id) {
		Optional<Product> vProductData = gIProductRepository.findById(id);
		if (vProductData.isPresent()) {
			try {
				Product vProduct = vProductData.get();
				return new ResponseEntity<>(vProduct, HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			Product vProductNull = new Product();
			return new ResponseEntity<>(vProductNull, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/exists/{productCode}")
	public ResponseEntity<Object> isExistsProductCode(@PathVariable String productCode) {
		try {
			boolean vProductCode = gIProductRepository.existsByProductCode(productCode);
			ExistsData vExistsData = new ExistsData();
			if (vProductCode) {
				vExistsData.setName(productCode);
				vExistsData.setExists(true);
			} else {
				vExistsData.setName(productCode);
				vExistsData.setExists(false);
			}
			return new ResponseEntity<>(vExistsData, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/create/{productLineId}")
	public ResponseEntity<Object> createProduct(@PathVariable Integer productLineId,
			@Valid @RequestBody Product paramProduct) {
		Optional<ProductLine> vProductLineData = gProductLineRepository.findById(productLineId);
		if (vProductLineData.isPresent()) {
			try {
				return new ResponseEntity<>(gProductService.createProduct(paramProduct, vProductLineData), HttpStatus.CREATED);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Create specified Product: " + e.getCause().getCause().getMessage());
			}
		} else {
			ProductLine vProductLineNull = new ProductLine();
			return new ResponseEntity<>(vProductLineNull, HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("/update/{id}/{productLineId}")
	public ResponseEntity<Object> updateProduct(@PathVariable Integer id, @PathVariable Integer productLineId,
			@Valid @RequestBody Product paramProduct) {
		Optional<Product> vProductData = gIProductRepository.findById(id);
		if (vProductData.isPresent()) {
			Optional<ProductLine> vProductLineData = gProductLineRepository.findById(productLineId);
			if (vProductLineData.isPresent()) {
				try {
					return new ResponseEntity<>(gProductService.updateOffice(paramProduct, vProductData, vProductLineData),
							HttpStatus.OK);
				} catch (Exception e) {
					return ResponseEntity.unprocessableEntity()
							.body("Failed to Update specified Product: " + e.getCause().getCause().getMessage());
				}
			} else {
				ProductLine vProductLineNull = new ProductLine();
				return new ResponseEntity<>(vProductLineNull, HttpStatus.NOT_FOUND);
			}
		} else {
			Product vProductNull = new Product();
			return new ResponseEntity<>(vProductNull, HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/delete/{id}")
	private ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
		Optional<Product> vProductData = gIProductRepository.findById(id);
		if (vProductData.isPresent()) {
			try {
				gIProductRepository.deleteById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			Product vProductNull = new Product();
			return new ResponseEntity<>(vProductNull, HttpStatus.NOT_FOUND);
		}
	}
}

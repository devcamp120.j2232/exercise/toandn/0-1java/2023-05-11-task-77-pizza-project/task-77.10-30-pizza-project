package com.devcamp.pizza365.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.*;
import com.devcamp.pizza365.model.ExistsData;
import com.devcamp.pizza365.repository.*;
import com.devcamp.pizza365.service.ProductLineService;

@RestController
@CrossOrigin
@RequestMapping("/product-line")
public class ProductLineController {
	@Autowired
	IProductLineRepository gProductLineRepository;

	@Autowired
	ProductLineService gProductLineService;

	@GetMapping("/all")
	public ResponseEntity<List<ProductLine>> getAllProductLine() {
		try {
			return new ResponseEntity<>(gProductLineService.getAllProductLine(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/details/{id}")
	public ResponseEntity<Object> getProductLineById(@PathVariable Integer id) {
		Optional<ProductLine> vProductLineData = gProductLineRepository.findById(id);
		if (vProductLineData.isPresent()) {
			try {
				ProductLine vProductLine = vProductLineData.get();
				return new ResponseEntity<>(vProductLine, HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			ProductLine vProductLineNull = new ProductLine();
			return new ResponseEntity<>(vProductLineNull, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/exists/{productLine}")
	public ResponseEntity<Object> isExistsProductLine(@PathVariable String productLine) {
		try {
			boolean vProductLine = gProductLineRepository.existsByProductLine(productLine);
			ExistsData vExistsData = new ExistsData();
			if (vProductLine) {
				vExistsData.setName(productLine);
				vExistsData.setExists(true);
			} else {
				vExistsData.setName(productLine);
				vExistsData.setExists(false);
			}
			return new ResponseEntity<>(vExistsData, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/create")
	public ResponseEntity<Object> createProductLine(@Valid @RequestBody ProductLine paramProductLine) {
		try {
			return new ResponseEntity<>(gProductLineService.createProductLine(paramProductLine), HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified ProductLine: " + e.getCause().getCause().getMessage());
		}

	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateProductLine(@PathVariable Integer id,
			@Valid @RequestBody ProductLine paramProductLine) {
		Optional<ProductLine> vProductLineData = gProductLineRepository.findById(id);
		if (vProductLineData.isPresent()) {
			try {
				return new ResponseEntity<>(gProductLineService.updateProductLine(paramProductLine, vProductLineData),
						HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified ProductLine: " + e.getCause().getCause().getMessage());
			}
		} else {
			ProductLine vProductLineNull = new ProductLine();
			return new ResponseEntity<>(vProductLineNull, HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/delete/{id}")
	private ResponseEntity<Object> deleteProductLineById(@PathVariable Integer id) {
		Optional<ProductLine> vProductLineData = gProductLineRepository.findById(id);
		if (vProductLineData.isPresent()) {
			try {
				gProductLineRepository.deleteById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			ProductLine vProductLineNull = new ProductLine();
			return new ResponseEntity<>(vProductLineNull, HttpStatus.NOT_FOUND);
		}
	}
}

package com.devcamp.pizza365.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.*;
import com.devcamp.pizza365.model.ExistsData;
import com.devcamp.pizza365.repository.*;
import com.devcamp.pizza365.service.EmployeeService;

@RestController
@CrossOrigin
@RequestMapping("/employee")
public class EmployeeController {
	@Autowired
	IEmployeeRepository gIEmployeeRepository;
	@Autowired
	EmployeeService gEmployeeService;

	@GetMapping("/all")
	public ResponseEntity<List<Employee>> getAllEmployee() {
		try {
			return new ResponseEntity<>(gEmployeeService.getAllEmployee(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/details/{id}")
	public ResponseEntity<Object> getEmployeeById(@PathVariable Integer id) {
		Optional<Employee> vEmployeeData = gIEmployeeRepository.findById(id);
		if (vEmployeeData.isPresent()) {
			try {
				Employee vEmployee = vEmployeeData.get();
				return new ResponseEntity<>(vEmployee, HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			Employee vEmployeeNull = new Employee();
			return new ResponseEntity<>(vEmployeeNull, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/exists/{email}")
	public ResponseEntity<Object> isExistsEmail(@PathVariable String email) {
		try {
			boolean vEmployee = gIEmployeeRepository.existsByEmail(email);
			ExistsData vExistsData = new ExistsData();
			if (vEmployee) {
				vExistsData.setName(email);
				vExistsData.setExists(true);
			} else {
				vExistsData.setName(email);
				vExistsData.setExists(false);
			}
			return new ResponseEntity<>(vExistsData, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/create")
	public ResponseEntity<Object> createEmployee(@Valid @RequestBody Employee paramEmployee) {
		try {
			return new ResponseEntity<>(gEmployeeService.createEmployee(paramEmployee), HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Employee: " + e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateEmployee(@PathVariable Integer id, @Valid @RequestBody Employee paramEmployee) {
		Optional<Employee> vEmployeeData = gIEmployeeRepository.findById(id);
		if (vEmployeeData.isPresent()) {
			try {
				return new ResponseEntity<>(gEmployeeService.updateEmployee(paramEmployee, vEmployeeData), HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Employee: " + e.getCause().getCause().getMessage());
			}
		} else {
			Employee vEmployeeNull = new Employee();
			return new ResponseEntity<>(vEmployeeNull, HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/delete/{id}")
	private ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
		Optional<Employee> vEmployeeData = gIEmployeeRepository.findById(id);
		if (vEmployeeData.isPresent()) {
			try {
				gIEmployeeRepository.deleteById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			Employee vEmployeeNull = new Employee();
			return new ResponseEntity<>(vEmployeeNull, HttpStatus.NOT_FOUND);
		}
	}
}

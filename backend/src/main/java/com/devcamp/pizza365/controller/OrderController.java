package com.devcamp.pizza365.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.repository.ICustomerRepository;
import com.devcamp.pizza365.repository.IOrderRepository;
import com.devcamp.pizza365.service.OrderService;

@RestController
@CrossOrigin
@RequestMapping("/orders")
public class OrderController {
  @Autowired
  IOrderRepository gIOrderRepository;
  @Autowired
  ICustomerRepository gICustomerRepository;
  @Autowired
  OrderService gOrderService;

  @GetMapping("/all")
  public ResponseEntity<List<Order>> getAllOrder() {
    try {
      return new ResponseEntity<>(gOrderService.getAllOrder(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/create/{customerId}")
  public ResponseEntity<Object> createOrder(@PathVariable Integer customerId,
      @Valid @RequestBody Order paramOrder) {
    Optional<Customer> vCustomerData = gICustomerRepository.findById(customerId);
    if (vCustomerData.isPresent()) {
      try {
        return new ResponseEntity<>(gOrderService.createOrder(paramOrder, vCustomerData), HttpStatus.CREATED);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Create specified Order: " + e.getCause().getCause().getMessage());
      }
    } else {
      Customer vCustomerNull = new Customer();
      return new ResponseEntity<>(vCustomerNull, HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping("/update/{id}/{CustomerId}")
  public ResponseEntity<Object> updateOrder(@PathVariable Integer id, @PathVariable Integer CustomerId,
      @Valid @RequestBody Order paramOrder) {
    Optional<Order> vOrderData = gIOrderRepository.findById(id);
    if (vOrderData.isPresent()) {
      Optional<Customer> vCustomerData = gICustomerRepository.findById(CustomerId);
      if (vCustomerData.isPresent()) {
        try {
          return new ResponseEntity<>(gOrderService.updateOrder(paramOrder, vOrderData, vCustomerData),
              HttpStatus.OK);
        } catch (Exception e) {
          return ResponseEntity.unprocessableEntity()
              .body("Failed to Update specified Order: " + e.getCause().getCause().getMessage());
        }
      } else {
        Customer vCustomerNull = new Customer();
        return new ResponseEntity<>(vCustomerNull, HttpStatus.NOT_FOUND);
      }
    } else {
      Order vOrderNull = new Order();
      return new ResponseEntity<>(vOrderNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteOrderById(@PathVariable Integer id) {
    Optional<Order> vOrderData = gIOrderRepository.findById(id);
    if (vOrderData.isPresent()) {
      try {
        gIOrderRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Order vOrderNull = new Order();
      return new ResponseEntity<>(vOrderNull, HttpStatus.NOT_FOUND);
    }
  }
}

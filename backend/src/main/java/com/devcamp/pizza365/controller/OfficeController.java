package com.devcamp.pizza365.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.*;
import com.devcamp.pizza365.repository.*;
import com.devcamp.pizza365.service.OfficeService;

@RestController
@CrossOrigin
@RequestMapping("/office")
public class OfficeController {
	@Autowired
	IOfficeRepository gOfficeRepository;
	@Autowired
	OfficeService gOfficeService;

	@GetMapping("/all")
	public ResponseEntity<List<Office>> getAllOffice() {
		try {
			return new ResponseEntity<>(gOfficeService.getAllOffice(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/details/{id}")
	public ResponseEntity<Object> getOfficeById(@PathVariable Integer id) {
		Optional<Office> vOfficeData = gOfficeRepository.findById(id);
		if (vOfficeData.isPresent()) {
			try {
				Office vOffice = vOfficeData.get();
				return new ResponseEntity<>(vOffice, HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			Office vOfficeNull = new Office();
			return new ResponseEntity<>(vOfficeNull, HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/create")
	public ResponseEntity<Object> createOffice(@Valid @RequestBody Office paramOffice) {
		try {
			return new ResponseEntity<>(gOfficeService.createEmployee(paramOffice), HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Office: " + e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateOffice(@PathVariable Integer id, @Valid @RequestBody Office paramOffice) {
		Optional<Office> vOfficeData = gOfficeRepository.findById(id);
		if (vOfficeData.isPresent()) {
			try {
				return new ResponseEntity<>(gOfficeService.updateOffice(paramOffice, vOfficeData), HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Office: " + e.getCause().getCause().getMessage());
			}
		} else {
			Office vOfficeNull = new Office();
			return new ResponseEntity<>(vOfficeNull, HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/delete/{id}")
	private ResponseEntity<Object> deleteOfficeById(@PathVariable Integer id) {
		Optional<Office> vOfficeData = gOfficeRepository.findById(id);
		if (vOfficeData.isPresent()) {
			try {
				gOfficeRepository.deleteById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			Office vOfficeNull = new Office();
			return new ResponseEntity<>(vOfficeNull, HttpStatus.NOT_FOUND);
		}
	}
}
